package com.company.physics;

public class Field {
    public static Integer width;
    public static Integer height;

    public static void preload(Integer width, Integer height) {
        Field.width = width;
        Field.height = height;
    }
}