package com.company.physics;

import com.company.tools.Rand;

public class PhysicalObject {
    public Double x;
    public Double y;

    public PhysicalObject() {
        x = Math.abs((double)Rand.rand.nextInt() % Field.width);
        y = Math.abs((double)Rand.rand.nextInt() % Field.height);
    }
    public PhysicalObject(PhysicalObject parent) {
        x = parent.x;
        y = parent.y;
    }

    //метод возвращающий растояние до объекта
    public Double distance(PhysicalObject object) {
        double dx = (object.x - this.x) % Field.width;
        double dy = (object.y - this.y) % Field.height;
        return Math.sqrt(dx * dx + dy * dy);
    }

}
