package com.company.physics;

import com.company.AI.AI;
import com.company.tools.Angle;

import javax.management.loading.MLetMBean;
import java.util.SimpleTimeZone;

public class Bacteria {
    //генетичекский код бактерии, и её физическая
    //составляющая, пока там только координаты, модуль
    //скорости и угол движения
    public AbleToMoveObject ableToMoveObject;
    public GeneticСode geneticСode;

    //по умолчанию 10, не зависит от того, сколько
    //затратил энергии на её создание её предок.
    public Double energy;

    //для изначальных бактерий, которые получаются случайным образом
    public Bacteria() {
        geneticСode = new GeneticСode();
        ableToMoveObject = new AbleToMoveObject(geneticСode.speed);
        energy = 15.0;
    }
    public Bacteria(Bacteria parent1, Bacteria parent2) {
        geneticСode = new GeneticСode(parent1.geneticСode, parent2.geneticСode);
        ableToMoveObject = new AbleToMoveObject(parent1.ableToMoveObject, geneticСode.speed);
        energy = 11.0;
    }

    public void move(Double dt){
        if (energy < 0) System.out.print("ENERGY_ERROR\n");
        energy -= 6 * dt;
        PhysicalObject target = calculatingTarget();
        step(target);

        eatBacteria();
        eatFood();
        reproduceBacteria();
    }

    private void calculateAngle(PhysicalObject target) {
        this.ableToMoveObject.angle = Angle.angle(target);
    }

    private void reproduceBacteria() {
        if (energy > 3 * (this.geneticСode.reproductionCost - this.geneticСode.reproductionDiscount)) {
            Level.addBacteria(this);
            this.energy -= this.geneticСode.reproductionCost - this.geneticСode.reproductionDiscount;
        }
    }

    private void eatFood() {
        for (int i = 0; i < Level.food.size(); i++)
            if (Level.food.get(i).distance(this.ableToMoveObject) <= 8) {
                Level.food.remove(i);
                this.energy += this.geneticСode.energyfood;
                break;
            }
    }

    private void eatBacteria() {
        for (int i = 0; i < Level.bacteria.size(); i++)
            if (Level.bacteria.get(i).ableToMoveObject.distance(this.ableToMoveObject) <= 24)
                if(!Level.bacteria.get(i).geneticСode.ID.equals(this.geneticСode.PID)
                        && !Level.bacteria.get(i).geneticСode.PID.equals(this.geneticСode.ID)
                        && !Level.bacteria.get(i).geneticСode.ID.equals(this.geneticСode.ID)
                        && !Level.bacteria.get(i).geneticСode.PID.equals(this.geneticСode.PID))
                    if (Level.bacteria.get(i).geneticСode.defense < this.geneticСode.offense){
                        Level.bacteria.remove(i);
                        Level.counterKills++;
                        this.energy += this.geneticСode.energykill + 10;
                        break;
                    }
    }

    private PhysicalObject calculatingTarget() {
        PhysicalObject target = AI.calculate(this);
        double angle = Angle.angle(target);
        this.ableToMoveObject.angle = angle + Math.PI / 2;
        if (target.x.equals(target.y) && target.y == 0) {
            target.x = this.geneticСode.defaultTarget.x;
            target.y = this.geneticСode.defaultTarget.y;
        }
        return target;
    }

    private void step(PhysicalObject target) {
        this.ableToMoveObject.x = (Field.width + this.ableToMoveObject.x + (((double)this.geneticСode.speed) / 3) * (target.x / Math.sqrt(target.x * target.x + target.y * target.y))) % Field.width;
        this.ableToMoveObject.y = (Field.height + this.ableToMoveObject.y + (((double)this.geneticСode.speed) / 3) * (target.y / Math.sqrt(target.x * target.x + target.y * target.y))) % Field.height;
    }


}
