package com.company.physics;

import com.company.tools.Rand;

import java.util.ArrayList;
import java.util.Collections;

import static java.lang.Double.NaN;

public class Level {
    private static Long dt;

    public static ArrayList<Bacteria> bacteria = new ArrayList<Bacteria>();
    public static ArrayList<Food> food = new ArrayList<Food>();

    public static Long counterKills;
    public static Long dtKills;

    public Level(Integer width, Integer height) {
        Field.preload(width, height);
        Level.dt = (long) 0;
        counterKills = (long) 0;
        dtKills = (long) 0;
        //TEST2();
        //TEST1();

        firstStart();
    }

    private void firstStart() {
        for (int i = 0; i < 100; i++){
            bacteria.add(new Bacteria());
            food.add(new Food());
            food.add(new Food());
        }
    }


    public static Integer update(long dt) {
        Level.dt += dt;
        Level.dtKills += dt;
        if (bacteriaUpdate(dt)) return 17102000;
        addFood();
        return 0;
    }

    private static void addFood() {
        if (Level.dt >= 8) {
            food.add(new Food());
            Level.dt -= 8;
        }
    }

    private static boolean bacteriaUpdate(double dt) {
        for (int i = 0; i < bacteria.size(); i++) {
            bacteria.get(i).move(dt / 1000.0);
            if (i == bacteria.size())
                continue;
            if (bacteria.get(i).energy < 0) {
                i = bacteriaDie(i);
            }
            if (bacteria.size() < 4) return true;
        }
        return false;
    }

    private static int bacteriaDie(int i) {
        food.add(new Food());
        food.get(food.size() - 1).x =bacteria.get(i).ableToMoveObject.x;
        food.get(food.size() - 1).y =bacteria.get(i).ableToMoveObject.y;
        bacteria.remove(i);
        i--;
        return i;
    }

    public static void addBacteria(Bacteria parent) {
        bacteria.add(new Bacteria(parent, bacteria.get(Math.abs(Rand.rand.nextInt() % bacteria.size()))));
        bacteria.get(bacteria.size() - 1).ableToMoveObject = new AbleToMoveObject(parent.ableToMoveObject, parent.ableToMoveObject.speed);
    }

    public static void reborn() {
        ArrayList<Bacteria> newGeneration = new ArrayList<Bacteria>();
        createNewGeneration(newGeneration);
        creatNewFood();
    }

    private static void creatNewFood() {
        food.removeAll(Collections.unmodifiableList(food));
        for (int i = 0; i < 150; i++) {
            food.add(new Food());
        }
    }

    private static void createNewGeneration(ArrayList<Bacteria> newGeneration) {
        for (int i = 0; i < 40; i++) {
            newGeneration.add(new Bacteria(bacteria.get(Math.abs(Rand.rand.nextInt() % bacteria.size())), bacteria.get(Math.abs(Rand.rand.nextInt() % bacteria.size()))));
            newGeneration.get(i).ableToMoveObject = new AbleToMoveObject(newGeneration.get(i).ableToMoveObject.speed);
        }
        bacteria = newGeneration;
    }



    private void TEST2() {
        Bacteria bac = new Bacteria();
        bac.ableToMoveObject.x = 350.;
        bac.ableToMoveObject.y = 250.;
        for (int i = 0; i < bac.geneticСode.synapseGroup1.size(); i++){
            bac.geneticСode.synapseGroup1.set(i, 0.);
        }
        for (int i = 0; i < bac.geneticСode.synapseGroup2.size(); i++){
            bac.geneticСode.synapseGroup2.set(i, 0.);
        }
        bac.geneticСode.synapseGroup1.set(0, 1.);
        bac.geneticСode.synapseGroup1.set(10, 1.);
        bac.geneticСode.synapseGroup2.set(0, 1.);
        bac.geneticСode.synapseGroup2.set(6, 1.);

        Food f = new Food();
        f.x = 250.;
        f.y = 250.;

        bacteria.add(bac);
        food.add(f);
    }

    private void TEST1() {
        Bacteria bac = new Bacteria();
        bac.ableToMoveObject.x = 350.;
        bac.ableToMoveObject.y = 250.;
        for (int i = 0; i < bac.geneticСode.synapseGroup1.size(); i++){
            bac.geneticСode.synapseGroup1.set(i, 0.);
        }
        for (int i = 0; i < bac.geneticСode.synapseGroup2.size(); i++){
            bac.geneticСode.synapseGroup2.set(i, 0.);
        }
        bacteria.add(bac);
    }
}
