package com.company.physics;

import com.company.tools.Rand;

public class AbleToMoveObject extends PhysicalObject{
    public Integer speed;
    public Double angle;

    public AbleToMoveObject(Integer speed){
        //конструктор для первых бактерий, которые не имеют предка
        super();
        this.angle = 0.0;
        this.speed = speed;
    }
    public AbleToMoveObject(AbleToMoveObject parent ,Integer speed){
        //создаём пердка в том же месте, что и родитель, но
        //надо будет потом сдвинуть обоих, года будут колизии
        super(parent);
        this.angle = parent.angle;
        this.speed = speed;
    }
}
