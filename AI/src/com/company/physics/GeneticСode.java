package com.company.physics;

import com.company.tools.Rand;

import java.util.ArrayList;
import java.util.Random;

public class GeneticСode{
    //"веса" синапсов нейронки
    //(ну а куда это ещё запихивать как не в генетический код)
    public ArrayList<Double> synapseGroup1 = new ArrayList<Double>(); //45
    public ArrayList<Double> synapseGroup2 = new ArrayList<Double>(); //10

    //стоимость в энергии создания потомка, бактреия будет создават
    //потомка только тогда, когда её энергии будет в 2 раза больше чем
    //кол-во энергии, которое требуется для создания потомка. И энергия
    //затраченная на это вычтется, а у потомка всегда будет 10 энергии
    //по умолчанию
    public Integer reproductionCost;

    public Integer ID = new Random().nextInt() % 100000;
    public Integer PID;

    //скорость поварота бактерии (как по часовой, так и против)
    //она будет смотреть а какую сторону быстрее повернуться
    //ЗНАЧЕНИЕ В ГРАДУСАХ (НАДО ПЕРЕВЕСТИ В РАДИАНЫ ПЕРЕД ВЫЧИСЛЕНИЯМИ)
    public Integer rotationSpeed;

    //радиус, в котором бактерия будет реагировать на объекты
    public Double radiusOfTheView;
    public PhysicalObject defaultTarget;
    //очки (в сумме 20)
    public Integer reproductionDiscount;
    public Integer energyfood;
    public Integer energykill;
    public Integer offense;
    public Integer defense;
    public Integer speed;

    public GeneticСode() {
        generateSynapseGroup1();
        generateSynapseGroup2();

        PID = 0;
        ID = new Random().nextInt() % 100000;

        //изначальная стоимость создания потомка
        reproductionCost = 20;
        rotationSpeed = 2000;
        radiusOfTheView = 250.;
        defaultTarget = new PhysicalObject();

        int points = 20;
        points = distributeSkills(points);

        //почему 47? 47 хромосом это вроде как какое-то
        //генетическое отклонение и в случае моей программы
        //быдет вызывать ассоциацию с гинетическим кодом
        if (points != 0) System.exit(47);
    }

    private Integer distributeSkills(Integer points) {
        points = generateSpeed(points);
        points = generateEnergyfood(points);
        points = generateEnergyKill(points);
        points = generateDefense(points);
        points = generateReproductionDiscount(points);
        //получает все остальные очки
        points = generateOffense(points);
        return points;
    }

    public GeneticСode(GeneticСode parent1, GeneticСode parent2) {
        this.copy(parent1, parent2);
        PID = parent1.ID;
        defaultTarget = new PhysicalObject();
        ID = new Random().nextInt() % 100000;
        if (Math.abs(Rand.rand.nextInt()) % 10 != 0)
            this.mutate();
    }

    private void copy(GeneticСode parent1, GeneticСode parent2) {
        copySynapseGroup1(parent1, parent2);
        copySynapseGroup2(parent1, parent2);
        copySkills(parent1);
    }

    private void copySynapseGroup2(GeneticСode parent1, GeneticСode parent2) {
        int a;
        for (int i = 0; i < parent1.synapseGroup2.size(); i++) {
            a = Rand.rand.nextInt();
            if (a > 0) synapseGroup2.add(parent1.synapseGroup2.get(i));
            else synapseGroup2.add(parent2.synapseGroup2.get(i));
        }
    }

    private void copySynapseGroup1(GeneticСode parent1, GeneticСode parent2) {
        int a;
        for (int i = 0; i < parent1.synapseGroup1.size(); i++) {
            a = Rand.rand.nextInt();
            if (a > 0) synapseGroup1.add(parent1.synapseGroup1.get(i));
            else synapseGroup1.add(parent2.synapseGroup1.get(i));
        }
    }

    private void copySkills(GeneticСode parent1) {
        reproductionDiscount = parent1.reproductionDiscount;
        reproductionCost = parent1.reproductionCost;
        speed = parent1.speed;
        offense = parent1.offense;
        defense = parent1.defense;
        energykill = parent1.energykill;
        energyfood = parent1.energyfood;
        radiusOfTheView = parent1.radiusOfTheView;
        rotationSpeed = parent1.rotationSpeed;
    }

    private void mutate() {
        //в чём тут суть? ну просто перераспределяем одно очко умений
        //и меняем значение двух синапсов (первой и второй группы)

        //ну да, это можно было сделать через массив, но кто же знал,
        //а когда додумался не хотел пол проги переписывать (да 10 строчек)
        //может когда-то я пределаю эту часть кода, например, когда мне
        //скажет преподаватель, что если это оставить так, то я не
        //получу автомат. Может когда-то я вернусь к этому проекту.
        if (!loseRandomPoint()) return;
        addRandomPoint();
        int pos;
        pos = Math.abs(Rand.rand.nextInt() % 45);
        synapseGroup1.set(pos, synapseGroup1.get(pos) * ((double)(Math.abs(Rand.rand.nextInt() % 1501) - 750)) / 620);
        pos = Math.abs(Rand.rand.nextInt() % 10);
        synapseGroup2.set(pos, synapseGroup2.get(pos) * ((double)(Math.abs(Rand.rand.nextInt() % 1501) - 750)) / 620);
    }

    private Boolean loseRandomPoint() {
        switch (Math.abs(Rand.rand.nextInt() % 5)){
            case(0) : if (speed == 0) return false; speed--; break;
            case(1) : if (offense == 0) return false; offense--; break;
            case(2) : if (defense == 0) return false; defense--; break;
            case(3) : if (energyfood == 0) return false; energyfood--; break;
            case(4) : if (energykill == 0) return false; energykill--; break;
        }
        return true;
    }

    private void addRandomPoint() {
        if (this.speed != 5){
            switch (Math.abs(Rand.rand.nextInt() % 5)){
                case(0) : speed++; break;
                case(1) : offense++; break;
                case(2) : defense++; break;
                case(3) : energyfood++; break;
                case(4) : energykill++; break;
            }
        } else {
            switch (Math.abs(Rand.rand.nextInt() % 4 + 1)){
                case(1) : offense++; break;
                case(2) : defense++; break;
                case(3) : energyfood++; break;
                case(4) : energykill++; break;
            }
        }
    }

    //если я правильно понял, то веса синепсов не должны быть больше
    //либо равными 1, а может я просто неправильно понял и я вернусь
    //к этому позже. А вот нихуя, вот я и вернулся, вес синапсов может
    //быть любым. Ебал я Диму в рот. Слушай после этого людей, которые
    //участвовали в комтехе.
    //В следующих методах просто заполняются случайные значения для
    //умений и синапсов.
    private void generateSynapseGroup1() {
        for (int i = 0; i < 45; i++) {
            synapseGroup1.add(((double) Rand.rand.nextInt() % 100) / 50);
        }
    }
    private void generateSynapseGroup2() {
        for (int i = 0; i < 10; i++) {
            synapseGroup2.add(((double) Rand.rand.nextInt() % 100) / 50);
        }
    }

    private Integer generateOffense(Integer points) {
        offense = points;
        if (offense < 0) {
            System.out.println("offense\n");
            //просто 47 + 1, почему 47 я писал выше, в других методах не может быть меньше нуля
            System.exit(48);
        }
        return points - offense;
    }
    private Integer generateSpeed(Integer points) {
        speed = Math.abs(Rand.rand.nextInt() % 5);
        return points - speed;
    }
    private Integer generateDefense(Integer points) {
        defense = Math.abs(Rand.rand.nextInt() % points);
        return points - defense;
    }
    private Integer generateEnergyfood (Integer points) {
        energyfood = Math.abs(Rand.rand.nextInt() % points);
        return points - energyfood;
    }
    private Integer generateEnergyKill (Integer points) {
        energykill = Math.abs(Rand.rand.nextInt() % points);
        return points - energykill;
    }
    private Integer generateReproductionDiscount (Integer points) {
        reproductionDiscount = Math.abs(Rand.rand.nextInt() % points);
        return points - reproductionDiscount;
    }

    // писал я всё это во время карантина 2020 года, у меня была куча долгов по
    // другим предметам, но мне это было интересно (а может я чуть-чуть сошёл с ума).
    // Хотя когда я вчера ходил в магазин я чувствовал себя неуютно на улице
}
