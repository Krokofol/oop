package com.company.graphics;

import com.company.physics.Level;

import java.security.Signature;
import java.util.Date;

public class MyTimer {
    private Integer dt;
    private Long prevKills;

    public MyTimer() {}

    public void start() throws InterruptedException {
        dt = 0;
        Level level = new Level(1768, 992);
        MyFrame myFrame = new MyFrame(1768, 992);
        while (true) {
            update(myFrame);
            output();
        }
    }

    private void update(MyFrame myFrame) throws InterruptedException {
        myFrame.draw();
        if(Level.update(6) == 17102000)
            Level.reborn();
        dt += 6;
        Thread.sleep(6);
    }

    private void output() {
        if (dt > 1000) {
            System.out.format("%d", Level.bacteria.size());
            dt -= 1000;
            if (Level.dtKills >= 5000) {
                System.out.format("\t %d", Level.counterKills);
                prevKills = Level.counterKills;
                Level.counterKills = (long) 0;
                Level.dtKills -= 5000;
            } else {
                System.out.format("\t %d", prevKills);
            }
            System.out.format("\n");
        }

    }

}
