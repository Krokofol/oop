package com.company.graphics;

import com.company.physics.Field;
import com.company.physics.Level;

import javax.swing.*;
import java.awt.*;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferStrategy;
import java.awt.image.ImageObserver;

public class MyFrame extends JFrame {

    private Image bacteriaImage;
    private ImageIcon bacteriaImageIcon;
    private ImageObserver bacteriaImageObserver;

    public MyFrame(Integer width, Integer height){
        bacteriaImageIcon = new ImageIcon("bacteria.png");
        bacteriaImageObserver = bacteriaImageIcon.getImageObserver();
        bacteriaImage = bacteriaImageIcon.getImage();

        this.setSize(width, height + 50);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setBackground(new Color(255, 255, 255));
        this.setVisible(true);
        this.createBufferStrategy(2);
    }

    public void draw() {
        BufferStrategy bufferStrat = this.getBufferStrategy();
        Graphics g1 = bufferStrat.getDrawGraphics();


        g1.clearRect(0, 0, Field.width, Field.height + 50);

        drawFood(g1);
        drawBacteria(g1);

        bufferStrat.show();
    }

    private void drawFood(Graphics g1) {
        g1.setColor(new Color(0, 162, 232));
        for (int i = 0; i < Level.food.size(); i++) {
            int x = (int) Math.round(Level.food.get(i).x);
            int y = (int) Math.round(Level.food.get(i).y);
            g1.fillOval(x - 3, y - 3 + 40, 6, 6);
        }
    }

    private void drawBacteria(Graphics g1) {
        for (int i = 0; i < Level.bacteria.size(); i++) {
            Integer x, y, r, g, b, sum;
            AffineTransform origXform;
            AffineTransform newXform;
            double angle = Level.bacteria.get(i).ableToMoveObject.angle;

            x = (int) Math.round(Level.bacteria.get(i).ableToMoveObject.x);
            y = (int) Math.round(Level.bacteria.get(i).ableToMoveObject.y) + 40;
            r = Level.bacteria.get(i).geneticСode.offense;
            g = Level.bacteria.get(i).geneticСode.speed;
            b = Level.bacteria.get(i).geneticСode.defense;
            sum = Math.max(r, Math.max(g ,b));
            r = 60 + r * 195 / sum;
            g = 60 + g * 195 / sum;
            b = 60 + b * 195 / sum;
            //g1.drawImage()
            g1.setColor(new Color(r, g, b));
            if (Level.bacteria.get(i).energy < 3)
                g1.setColor(Color.RED);
            origXform = ((Graphics2D) g1).getTransform();
            newXform = ((Graphics2D) g1).getTransform();
            newXform.rotate(Math.PI / 2 - angle, x, y);
            ((Graphics2D) g1).setTransform(newXform);
            g1.fillOval(x - 6, y - 12, 12, 23);
            ((Graphics2D) g1).setTransform(origXform);

            x = (int) Math.round(Level.bacteria.get(i).ableToMoveObject.x);
            y = (int) Math.round(Level.bacteria.get(i).ableToMoveObject.y) + 40;
            origXform = ((Graphics2D) g1).getTransform();
            newXform = ((Graphics2D) g1).getTransform();
            newXform.rotate(Math.PI / 2 - angle, x, y);
            ((Graphics2D) g1).setTransform(newXform);
            ((Graphics2D) g1).drawImage(bacteriaImage, x - 9, y - 15, 17, 30, null);
            ((Graphics2D) g1).setTransform(origXform);
        }
    }

}
