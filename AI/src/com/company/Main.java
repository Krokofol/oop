package com.company;

import com.company.graphics.MyTimer;

public class Main {
    public static void main(String[] args) throws InterruptedException {
        MyTimer myTimer = new MyTimer();
        myTimer.start();
    }
}
