package com.company.AI;

import com.company.physics.Bacteria;
import com.company.physics.Field;
import com.company.physics.Level;
import com.company.physics.PhysicalObject;

import java.util.ArrayList;
import java.util.Date;

public class AI {

    //ну если эта штука сможет чему-то научиться это будет просто фантастика
    //я примерно понимаю как оно должно работать, но не особо представляю,
    //чтобы эти 100 строк кода смогли подсказать этому тупому кружочку куда
    //идти стоит, а куда нет. Я конечно постараюсь это дописать как можно
    //скорей и поставить уже на эту ночь обучаться

    public static PhysicalObject calculate(Bacteria bacteria){
        // возвращаемые нейронкой координаты, к которым
        // будет стремиться наша бактерия (тут только X и Y)
        PhysicalObject physicalObject = new PhysicalObject();

        // сюда мы запишем входные данные для нашей нейронки
        // будет специальный метод тут, который их посчитает
        ArrayList<Double> input = new ArrayList<Double>();
        ArrayList<Double> hidenLevel = new ArrayList<Double>();

        //метод считающий весь input
        calculateInput(input, bacteria);
        //теперь надо посчить скрытый уровень нейронов
        calculateHidenLevel(input, hidenLevel, bacteria);
        //теперь считаем координаты output
        calculateOutput(hidenLevel, bacteria, physicalObject);

        return physicalObject;
    };

    private static void calculateOutput(ArrayList<Double> hidenLevel, Bacteria bacteria, PhysicalObject output) {
        output.x = 0.;
        output.y = 0.;
        for (int i = 0; i < hidenLevel.size(); i += 2) {
            output.x += hidenLevel.get(i) * bacteria.geneticСode.synapseGroup2.get(i);
            output.y += hidenLevel.get(i) * bacteria.geneticСode.synapseGroup2.get(i + 1);
        }
    }

    private static void calculateHidenLevel(ArrayList<Double> input, ArrayList<Double> hidenLevel, Bacteria bacteria){
        for (int j = 0; j < bacteria.geneticСode.synapseGroup1.size() / input.size(); j++)
            hidenLevel.add(0.0);
        for (int i = 0; i < input.size(); i++)
            for (int j = 0; j < bacteria.geneticСode.synapseGroup1.size() / input.size(); j++)
                hidenLevel.set(j, hidenLevel.get(j) + input.get(i) * bacteria.geneticСode.synapseGroup1.get(j * input.size() + i));
    }

    // метод, который считает весь input
    // там будет 9 нейронов
    //      0 - средняя координата x видимой еды
    //      1 - средняя координата y видимой еды
    //      2 - средняя координата x не опасной бактерии
    //      3 - средняя координата y не опасной бактерии
    //      4 - средняя координата x опасной бактерии
    //      5 - средняя координата y опасной бактерии
    //      6 - срденяя координата x съедобной бактерии
    //      7 - средеяя координата y съедобной бактерии
    //      8 - sin(t), я посмотрел, это для большего разнообразия
    private static void calculateInput(ArrayList<Double> input, Bacteria bacteria){
        calculateInputFoodMass(input, bacteria);
        calculateInputBactMass(input, bacteria);
        input.add(Math.sin((double)(new Date().getTime() % 100) / 10));
    }

    // метод, который считает центр масс видемой еды
    // если она не видет еду, то координатам присваиваются
    // значение координат самой бактерии
    private static void calculateInputFoodMass(ArrayList<Double> input, Bacteria bacteria) {
        input.add(0.0);
        input.add(0.0);
        for (int i = 0; i < Level.food.size(); i++)
            if (bacteria.ableToMoveObject.distance(Level.food.get(i)) <= bacteria.geneticСode.radiusOfTheView) {
                input.set(0, input.get(0) + (Level.food.get(i).x - bacteria.ableToMoveObject.x) * (bacteria.geneticСode.radiusOfTheView - bacteria.ableToMoveObject.distance(Level.food.get(i))) * (bacteria.geneticСode.radiusOfTheView - bacteria.ableToMoveObject.distance(Level.food.get(i))) * 4);
                input.set(1, input.get(1) + (Level.food.get(i).y - bacteria.ableToMoveObject.y) * (bacteria.geneticСode.radiusOfTheView - bacteria.ableToMoveObject.distance(Level.food.get(i))) * (bacteria.geneticСode.radiusOfTheView - bacteria.ableToMoveObject.distance(Level.food.get(i))) * 4);
            }
    }

    //метод, считающий среднюю массу безопасных бактерий
    //метод, считающий среднюю массу опасных бактерий
    //метод, считающий среднюю массу съедобных бактерий
    private static void calculateInputBactMass(ArrayList<Double> input, Bacteria bacteria) {
        int countSafe = 0;
        int countDang = 0;
        int countFood = 0;
        for (int i = 0; i < 6; i++)
            input.add(0.0);
        for (int i = 0; i < Level.bacteria.size(); i++)
            if (bacteria.ableToMoveObject.distance(Level.bacteria.get(i).ableToMoveObject) < bacteria.geneticСode.radiusOfTheView) {
                if (bacteria.geneticСode.defense < Level.bacteria.get(i).geneticСode.offense
                        && ableToKill(bacteria, i)) {
                    input.set(4, input.get(4) + (Level.bacteria.get(i).ableToMoveObject.x - bacteria.ableToMoveObject.x) * (bacteria.geneticСode.radiusOfTheView - bacteria.ableToMoveObject.distance(Level.bacteria.get(i).ableToMoveObject)) * (bacteria.geneticСode.radiusOfTheView - bacteria.ableToMoveObject.distance(Level.bacteria.get(i).ableToMoveObject)) * 4);
                    input.set(5, input.get(5) + (Level.bacteria.get(i).ableToMoveObject.y - bacteria.ableToMoveObject.y) * (bacteria.geneticСode.radiusOfTheView - bacteria.ableToMoveObject.distance(Level.bacteria.get(i).ableToMoveObject)) * (bacteria.geneticСode.radiusOfTheView - bacteria.ableToMoveObject.distance(Level.bacteria.get(i).ableToMoveObject)) * 4);
                }
                else {
                    input.set(2, input.get(2) + (Level.bacteria.get(i).ableToMoveObject.x - bacteria.ableToMoveObject.x) * (bacteria.geneticСode.radiusOfTheView - bacteria.ableToMoveObject.distance(Level.bacteria.get(i).ableToMoveObject)) * (bacteria.geneticСode.radiusOfTheView - bacteria.ableToMoveObject.distance(Level.bacteria.get(i).ableToMoveObject))*4);
                    input.set(3, input.get(3) + (Level.bacteria.get(i).ableToMoveObject.y - bacteria.ableToMoveObject.y) * (bacteria.geneticСode.radiusOfTheView - bacteria.ableToMoveObject.distance(Level.bacteria.get(i).ableToMoveObject)) * (bacteria.geneticСode.radiusOfTheView - bacteria.ableToMoveObject.distance(Level.bacteria.get(i).ableToMoveObject))*4);
                }
                if (bacteria.geneticСode.offense > Level.bacteria.get(i).geneticСode.defense
                        && ableToKill(bacteria, i)) {
                    input.set(6, input.get(6) + (Level.bacteria.get(i).ableToMoveObject.x - bacteria.ableToMoveObject.x) * (bacteria.geneticСode.radiusOfTheView - bacteria.ableToMoveObject.distance(Level.bacteria.get(i).ableToMoveObject)) * (bacteria.geneticСode.radiusOfTheView - bacteria.ableToMoveObject.distance(Level.bacteria.get(i).ableToMoveObject))*4);
                    input.set(7, input.get(7) + (Level.bacteria.get(i).ableToMoveObject.y - bacteria.ableToMoveObject.y) * (bacteria.geneticСode.radiusOfTheView - bacteria.ableToMoveObject.distance(Level.bacteria.get(i).ableToMoveObject)) * (bacteria.geneticСode.radiusOfTheView - bacteria.ableToMoveObject.distance(Level.bacteria.get(i).ableToMoveObject))*4);
                }
            }
    }

    private static boolean ableToKill(Bacteria bacteria, int i) {
        return !Level.bacteria.get(i).geneticСode.ID.equals(bacteria.geneticСode.PID)
                && !Level.bacteria.get(i).geneticСode.PID.equals(bacteria.geneticСode.ID)
                && !Level.bacteria.get(i).geneticСode.ID.equals(bacteria.geneticСode.ID)
                && !Level.bacteria.get(i).geneticСode.PID.equals(bacteria.geneticСode.PID);
    }
}
