package com.company.tools;

import com.company.physics.AbleToMoveObject;
import com.company.physics.PhysicalObject;

public class Angle {

    public static Double angle(PhysicalObject target) {
        if (target.x > 0 && target.y > 0)
            return Math.abs(Math.asin(target.x / Math.sqrt(target.x * target.x + target.y * target.y)));
        if (target.x < 0 && target.y > 0)
            return Math.PI - Math.abs(Math.asin(target.x / Math.sqrt(target.x * target.x + target.y * target.y)));
        if (target.x < 0 && target.y < 0)
            return -Math.PI + Math.abs(Math.asin(target.x / Math.sqrt(target.x * target.x + target.y * target.y)));
        if (target.x > 0 && target.y < 0)
            return -Math.abs(Math.asin(target.x / Math.sqrt(target.x * target.x + target.y * target.y)));
        return 0.0;
    }
}
