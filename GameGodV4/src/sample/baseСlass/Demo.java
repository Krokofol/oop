package sample.baseСlass;

import javax.swing.*;
import java.awt.*;

public class Demo extends JPanel {

    public static Integer rectSize = 100;

    public void paintComponent (Graphics g) {
        super.paintComponent(g);
        for (int i = 0; i < PhysicalObject.field.size(); i++) {
            for (int j = 0; j < PhysicalObject.field.size(); j++) {
                switch (PhysicalObject.field.get(i).get(j)) {
                    case (0):
                        g.setColor(Color.getHSBColor(101, 100, 100));
                        break;
                    case (1):
                        g.setColor(Color.getHSBColor(101, 500000, 350));
                        break;
                    case (2):
                        g.setColor(Color.PINK);
                        break;
                }
                g.fillRect(i * 100, j * 100, (i + 1) * 100, (j + 1) * 100);
            }
        }
    }

}
