package sample.baseСlass;

import java.util.ArrayList;
import java.util.Random;

public class PhysicalObject {
    //сначала координата X(ограниченная width), потом координата Y(ограниченная height)
    public static ArrayList<ArrayList<Integer>> field;
    public static Integer width = 0;
    public static Integer height = 0;
    private Integer x = 0;
    private Integer y = 0;

    private static Random random = new Random();
    private static Integer getInteger(Integer max){
        return Math.abs(random.nextInt() % max) ;
    }

    public static void clear(){
        for (int i = 0; i < width; i++)
            for (int j = 0; j < height; j++)
                field.get(i).set(j, 0);
    }

    public PhysicalObject(Integer type){
        x = getInteger(width);
        y = getInteger(height);
        while (PhysicalObject.field.get(x).get(y) != 0){
            x = getInteger(width);
            y = getInteger(height);
        }
        field.get(x).set(y, type);
    }

    public static void preload(Integer width, Integer height){
        PhysicalObject.width = width;
        PhysicalObject.height = height;
        field = new ArrayList<ArrayList<Integer>>();
        for (int i = 0; i < width; i++){
            field.add(new ArrayList<Integer>());
            for (int j = 0; j < height; j++) {
                field.get(i).add(0);
            }
        }
    }

    public void setX(Integer x) {
        this.x = x;
    }
    public void setY(Integer y) {
        this.y = y;
    }

    public Integer getX() {
        return x;
    }
    public Integer getY() {
        return y;
    }

    public static Integer cheak(Integer x, Integer y){
        return field.get(x).get(y);
    }

}
