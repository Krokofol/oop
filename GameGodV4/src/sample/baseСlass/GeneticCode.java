package sample.baseСlass;

import sample.classes.Bacteria;

import java.io.File;
import java.util.ArrayList;
import java.util.Random;

public class GeneticCode {
    public static Integer size = 100; //размер массива действий
    private ArrayList<Integer> code = new ArrayList<Integer>(); //коды классов действий, наследников action

    private static Integer actionsNumber = 0; //кол-во действий
    private static ArrayList<String> actions = new ArrayList<String>(); //названия классов действий, наследников action

    public Integer iteration = 0; //итерация, действие, на котором сейчас находится
    public Integer nextIteration = -1; //следующее действие, если удёт по порядку, то -1

    private static Random random = new Random();
    private static Integer getInteger(Integer max){
        return Math.abs(random.nextInt() % max) ;
    }

    //заполнение действий из дирректории actions
    public static void preload(){
        File folder = new File("src/sample/actions");
        String[] files = folder.list();
        for(String s1 : files){
            addAction(s1.substring(0, s1.length() - 5));
        }
    }

    //констурктор класса генетического кода по умолчанию
    public GeneticCode(){
        for (int i = 0; i < size; i++){
            code.add(getInteger(actionsNumber));
        }
    }

    //добавление названия класса действия в массив
    public static void addActions(String[] actions){
        for (String s : actions){
            GeneticCode.addAction(s);
        }
    }
    private static void addAction(String action){
        for (String s : actions) {
            if (s.equals(action)) return;
        }
        actions.add("sample.actions." + action);
        actionsNumber++;
    }

    //выполнение действия, каждое действие тратит 1 энергии
    public void update(Bacteria bacteria){
        MyAction myAction = null;
        try {
            Class clazz = Class.forName(actions.get(iteration));
            myAction = (MyAction) clazz.newInstance();
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException e) {
            e.printStackTrace();
        }
        assert myAction != null;
    }


}
