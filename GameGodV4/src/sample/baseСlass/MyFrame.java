package sample.baseСlass;

import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferStrategy;

public class MyFrame extends JFrame {
    public static Integer rectSize = 6;

    public MyFrame(Integer w, Integer h){
        this.setSize(w * rectSize + 25, h * rectSize + 32);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setBackground(Color.getHSBColor(101, 100, 100));
        this.setVisible(true);
        this.createBufferStrategy(2);
    }

    public void draw() {
        BufferStrategy bufferStrat = this.getBufferStrategy();
        Graphics g1 = bufferStrat.getDrawGraphics();
        for(int i = 0; i < PhysicalObject.field.size(); i++) {
            for (int j = 0; j < PhysicalObject.field.get(0).size(); j++) {
                switch (PhysicalObject.field.get(i).get(j)) {
                    case (0):
                        g1.setColor(new Color(248, 254, 122));
                        break;
                    case (1):
                        g1.setColor(new Color(10, 105, 222));
                        break;
                    case (2):
                        g1.setColor(new Color(89, 170, 10));
                        break;
                }
                g1.fillRect(i * rectSize + 15, j * rectSize + 32, (i + 1) * rectSize + 15, (j + 1) * rectSize + 32);
            }
        }
        bufferStrat.show();
    }
}
