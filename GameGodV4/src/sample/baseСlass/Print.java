package sample.baseСlass;

public class Print {
    public static void print(){
        int bacteriaCount = 0;
        int foodCount = 0;
        for (int j = 0; j < PhysicalObject.height; j++){
            for (int i = 0; i < PhysicalObject.width; i++){
                System.out.print(PhysicalObject.field.get(i).get(j));
                if (PhysicalObject.field.get(i).get(j) == 1) foodCount++;
                if (PhysicalObject.field.get(i).get(j) == 2) bacteriaCount++;
                System.out.print(' ');
            }
            System.out.print('\n');
        }
        System.out.print(foodCount);
        System.out.print('\n');
        System.out.print(bacteriaCount);
        System.out.print('\n');
    }
}
