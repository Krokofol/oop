package sample.actions;

import sample.classes.GeneticCode;
import sample.baseСlass.MyAction;
import sample.classes.Bacteria;

public class TurnRight extends MyAction {
    @Override
    public Integer action(Bacteria bacteria){
        bacteria.setView('R');
        bacteria.setEnergy(bacteria.getEnergy() - 1);
        if (bacteria.getGeneticCode().nextIteration != -1){
            bacteria.getGeneticCode().iteration = bacteria.getGeneticCode().nextIteration;
            bacteria.getGeneticCode().nextIteration = -1;
        }
        else bacteria.getGeneticCode().iteration = (bacteria.getGeneticCode().iteration + 1) % GeneticCode.size;
        return 0;
    }
}
