package sample.actions;

import sample.classes.GeneticCode;
import sample.baseСlass.MyAction;
import sample.baseСlass.PhysicalObject;
import sample.classes.Bacteria;

public class Check extends MyAction {
    @Override
    public Integer action(Bacteria bacteria){
        switch (check(bacteria)){
            case (0) : itIsEmpty(bacteria); break;
            case (1) : itIsBacteria(bacteria); break;
            case (2) : itIsFood(bacteria); break;
            default  : bacteria.getGeneticCode().iteration = (bacteria.getGeneticCode().iteration + 4) % GeneticCode.size;
                       bacteria.getGeneticCode().nextIteration = (bacteria.getGeneticCode().iteration + 5) % GeneticCode.size;
                       break;
        }
        bacteria.setEnergy(bacteria.getEnergy() - 1);
        return 0;
    }

    private Integer check(Bacteria bacteria) {
        switch (bacteria.getView()){
            case('U') : return checkUp(bacteria);
            case('D') : return checkDown(bacteria);
            case('R') : return checkRight(bacteria);
            case('L') : return checkLeft(bacteria);
        }

        return 0;
    }

    private Integer checkUp(Bacteria bacteria){
        if (bacteria.getY() - 1 < 0) return -1;
        return PhysicalObject.field.get(bacteria.getX()).get(bacteria.getY() - 1);
    }
    private Integer checkDown(Bacteria bacteria){
        if (bacteria.getY() + 1 >= PhysicalObject.height) return -1;
        return PhysicalObject.field.get(bacteria.getX()).get(bacteria.getY() + 1);
    }
    private Integer checkLeft(Bacteria bacteria){
        if (bacteria.getX() - 1 < 0) return -1;
        return PhysicalObject.field.get(bacteria.getX() - 1).get(bacteria.getY());
    }
    private Integer checkRight(Bacteria bacteria){
        if (bacteria.getX() + 1 >= PhysicalObject.width) return -1;
        return PhysicalObject.field.get(bacteria.getX() + 1).get(bacteria.getY());
    }

    private void itIsFood(Bacteria bacteria){
        bacteria.getGeneticCode().nextIteration = (bacteria.getGeneticCode().iteration + 4) % GeneticCode.size;
        bacteria.getGeneticCode().iteration = (bacteria.getGeneticCode().iteration + 1) % GeneticCode.size;
    }
    private void itIsBacteria(Bacteria bacteria){
        bacteria.getGeneticCode().nextIteration = (bacteria.getGeneticCode().iteration + 4) % GeneticCode.size;
        bacteria.getGeneticCode().iteration = (bacteria.getGeneticCode().iteration + 2) % GeneticCode.size;
    }
    private void itIsEmpty(Bacteria bacteria){
        bacteria.getGeneticCode().nextIteration = (bacteria.getGeneticCode().iteration + 4) % GeneticCode.size;
        bacteria.getGeneticCode().iteration = (bacteria.getGeneticCode().iteration + 3) % GeneticCode.size;
    }

}
