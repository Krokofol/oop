package sample.actions;

import sample.classes.GeneticCode;
import sample.baseСlass.MyAction;
import sample.baseСlass.PhysicalObject;
import sample.classes.Bacteria;
import sample.classes.Level;

public class Eat extends MyAction {
    @Override
    public Integer action(Bacteria bacteria){
        PhysicalObject.field.get(bacteria.getX()).set(bacteria.getY(), 0);
        //Print.print();
        switch (bacteria.getView()){
            case('U') : GoUp(bacteria); break;
            case('D') : GoDown(bacteria); break;
            case('L') : GoLeft(bacteria); break;
            case('R') : GoRight(bacteria); break;
        }
        bacteria.setEnergy(bacteria.getEnergy() - 1);
        if (bacteria.getGeneticCode().nextIteration != -1){
            bacteria.getGeneticCode().iteration = bacteria.getGeneticCode().nextIteration;
            bacteria.getGeneticCode().nextIteration = -1;
        }
        else bacteria.getGeneticCode().iteration = (bacteria.getGeneticCode().iteration + 1) % GeneticCode.size;
        PhysicalObject.field.get(bacteria.getX()).set(bacteria.getY(), 1);
        //Print.print();
        return 0;
    }

    private static void killFood(Bacteria bacteria) {
        for (int i = 0; i < Level.foodNumber; i++){
            if (bacteria.getX().equals(Level.food.get(i).getX()) && bacteria.getY().equals(Level.food.get(i).getY())) {
                Level.food.remove(i);
                Level.foodNumber--;
                bacteria.setEnergy(bacteria.getEnergy() + 7);
                return;
            }
        }
    }

    private static void GoUp(Bacteria bacteria){
        int x = bacteria.getX();
        int y = bacteria.getY();
        switch (PhysicalObject.cheak(x, (y - 1 + PhysicalObject.height) % PhysicalObject.height)){
            case(0):
                bacteria.setY((y - 1 + PhysicalObject.height) % PhysicalObject.height); break;
            case(2):
                bacteria.setY((y - 1 + PhysicalObject.height) % PhysicalObject.height); break;
        }
        Eat.killFood(bacteria);
    }
    private static void GoDown(Bacteria bacteria){
        int x = bacteria.getX();
        int y = bacteria.getY();
        switch (PhysicalObject.cheak(x, (y + 1 + PhysicalObject.height) % PhysicalObject.height )){
            case(0):
                bacteria.setY((y + 1 + PhysicalObject.height) % PhysicalObject.height); break;
            case(2):
                bacteria.setY((y + 1 + PhysicalObject.height) % PhysicalObject.height); break;
        }
        Eat.killFood(bacteria);
    }

    private static void GoLeft(Bacteria bacteria){
        int x = bacteria.getX();
        int y = bacteria.getY();
        switch (PhysicalObject.cheak((x - 1 + PhysicalObject.width) % PhysicalObject.width, y)){
            case(0):
                bacteria.setX((x - 1 + PhysicalObject.width) % PhysicalObject.width); break;
            case(2):
                bacteria.setX((x - 1 + PhysicalObject.width) % PhysicalObject.width); break;
        }
        Eat.killFood(bacteria);
    }
    private static void GoRight(Bacteria bacteria){
        int x = bacteria.getX();
        int y = bacteria.getY();
        switch (PhysicalObject.cheak((x + 1 + PhysicalObject.width) % PhysicalObject.width, y)){
            case(0):
                bacteria.setX((x + 1 + PhysicalObject.width) % PhysicalObject.width); break;
            case(2):
                bacteria.setX((x + 1 + PhysicalObject.width) % PhysicalObject.width); break;
        }
        Eat.killFood(bacteria);
    }
}
