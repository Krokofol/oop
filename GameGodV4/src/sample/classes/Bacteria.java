package sample.classes;

import sample.baseСlass.PhysicalObject;

public class Bacteria extends PhysicalObject {
    private GeneticCode geneticCode;
    private Integer energy = 10;
    private char view = 'U';

    public Bacteria(){
        super(1);
        geneticCode = new GeneticCode();
    }

    public Bacteria (Bacteria parent1, Boolean mutation) {
        super(1);
        geneticCode = new GeneticCode(parent1.geneticCode, mutation);
    }

    public void die(){
        PhysicalObject.field.get(this.getX()).set(this.getY(), 0);
    }

    public char getView() {
        return view;
    }
    public void setView (char newTurn){
        view = newTurn;
    }

    public Integer getEnergy() { return energy; }
    public void setEnergy(Integer energy) { this.energy = energy; }

    public GeneticCode getGeneticCode(){ return geneticCode; }

    public boolean update(){
        geneticCode.update(this);
        return this.energy > 0;
    }
}
