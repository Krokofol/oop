package sample.classes;

import sample.baseСlass.MyFrame;
import sample.baseСlass.PhysicalObject;

import java.util.ArrayList;

public class Level {
    public static ArrayList<Bacteria> bacteria = new ArrayList<Bacteria>();
    public static ArrayList<Food> food = new ArrayList<Food>();
    public static Integer foodNumberMax;
    public static Integer foodNumber;
    public static Integer bacteriaNumber;
    public static Integer bacteriaNumberMax;

    public static void preload(Integer width, Integer height){
        PhysicalObject.preload(width, height);
        GeneticCode.preload();
        bacteriaNumberMax = Math.max(PhysicalObject.height, PhysicalObject.width);
        foodNumberMax = PhysicalObject.width * PhysicalObject.height * 21 / 120;
    }

    public Level() {
        for (bacteriaNumber = 0; bacteriaNumber < bacteriaNumberMax; bacteriaNumber++) {
            bacteria.add(new Bacteria());
        }
        for (foodNumber = 0; foodNumber < foodNumberMax; foodNumber++){
            food.add(new Food());
        }
    }

    public Integer update(){
        for (int i = 0; i < bacteriaNumber; i++) {
            if (!bacteria.get(i).update()) {
                bacteria.get(i).die();
                bacteria.remove(i);
                bacteriaNumber--;
                i--;
            }
            if (bacteriaNumber <= Math.sqrt(bacteriaNumberMax))
                return 13072000;
        }
        for (int i = foodNumber; i < foodNumberMax; i++)
            food.add(new Food());
        foodNumber = foodNumberMax;
        return 0;
    }

    public void reborn(){

        PhysicalObject.clear();

        ArrayList<Bacteria> newGeneration= new ArrayList<Bacteria>();
        for (int i = 0; i < bacteriaNumber; i++)
            for (int j = 0; j < bacteriaNumber; j++)
                newGeneration.add(new Bacteria(bacteria.get(i), (bacteriaNumber / 2) >= j));

        for (int i = bacteriaNumber * bacteriaNumber; i < bacteriaNumberMax; i++)
            newGeneration.add(new Bacteria());

        ArrayList<Food> newFood = new ArrayList<Food>();
        for (foodNumber = 0; foodNumber < foodNumberMax; foodNumber++){
            newFood.add(new Food());
        }
        bacteriaNumber = bacteriaNumberMax;
        food = newFood;
        bacteria = newGeneration;
    }

    public void go(Boolean IsItShouldSleep, MyFrame myFrame) throws InterruptedException {
        int counter = 0;
        int max = 0;

        do {
            if (this.update() == 13072000) {
                counter++;
                if (max < counter) max = counter;
                System.out.print(counter);
                System.out.print('\n');
                counter = 0;
                myFrame.draw();
                if (IsItShouldSleep)
                    Thread.sleep(200);
                this.reborn();
            }
            counter++;
            myFrame.draw();
            if (IsItShouldSleep)
                Thread.sleep(17);
        } while (true);
    }


}
