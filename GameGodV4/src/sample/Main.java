package sample;

import sample.baseСlass.MyFrame;
import sample.classes.Level;

public class Main {

    public static void main(String[] args) throws InterruptedException {
        Integer fieldXSize = 100, fieldYSize = 100;
        Boolean sleep = true;

        Level.preload(fieldXSize, fieldYSize);
        Level level = new Level();
        MyFrame myFrame = new MyFrame(fieldXSize, fieldYSize);

        level.go(sleep, myFrame);
    }
}
