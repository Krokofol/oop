package sample.classes;

import java.util.ArrayList;

class Algorithm {
    //шаги алгоритма
    private ArrayList<Integer> index;
    //массив действий
    private ArrayList<Integer> actions;

    //генератор случайных положительных чисел
    final Rand rand = new Rand();

    /*
    /////////////////////////////////////////////////////////////////////////////////
    0 - проверяет, что в след. клетке       не тратит энергию
        если в следующей клетке
            еда     : перейти к действию 1, а затем к 4
            бактерия: перейти к действию 2, а затем к 4
            пусто   : перейти к действию 3, а затем к 4

    1 - повернуться вверх,                  не тратит энергию
    2 - повернуться вправо,                 не тратит энергию
    3 - повернуться вниз,                   не тратит энергию
    4 - повернуться влево,                  не тратит энергию

    5 - шагнуть в следующую клетку          тратит энергию (1)
    6 - съедает пищу из следующей клетки    тратит энергию (1)
    7 - съедает бактерию из след. клетки    тратит энергию (1)

    8 - проверяет, может ли размножиться    не тратит энергию
        если энергия
            больше 20 : перейти к действию 1, затем к 2
            меньше 21 : перейти к действию 2
    /////////////////////////////////////////////////////////////////////////////////
    */

    //создание алгоритма
    public Algorithm(){
        for (int i = 0; i < 4; i++){
            this.index.add(i);
        }
        actions = new ArrayList<Integer>();
        for (int i = 0; i < 10; i++) {
            this.actions.add(rand.getInteger(8));
        }
    }
    //наследование от двух алгоритмов
    public Algorithm(Algorithm a1, Algorithm a2){
        for (int i = 0; i < 4; i++){
            this.index.add(i);
        }
        actions = new ArrayList<Integer>();
        for (int i = 0; i < 10; i++) {
            if (rand.getInteger(2) == 1){
                actions.add(a1.actions.get(i));
            }
            else{
                actions.add(a2.actions.get(i));
            }
        }
    }

    public Integer getAction() { return actions.get(index.get(0)); }
    public Integer getThisIndex() { return index.get(0); }
    public void moveIndex(){
        for (int i = 0; i < 3; i++){
            this.index.set(i, this.index.get(i + 1));
        }
        this.index.set(3, (this.index.get(3) + 1) % 10);
    }
    public void setIndex(ArrayList<Integer> newIndex){
        for (int i = 0; i < 4; i++){
            this.index.set(i, newIndex.get(i));
        }
    }
}
