package sample.classes;

class PhysicalObject {
    //столбец таблицы
    private Integer X;
    //строка таблицы
    private Integer Y;

    public Integer getY() { return Y; }
    public Integer getX() { return X; }
    public void setY(Integer y) { Y = y; }
    public void setX(Integer x) { X = x; }
}
