package sample.classes;

import java.util.Random;

public class Rand {
    //гениратор случайных чисел
    final Random random = new Random();

    public Integer getInteger(Integer max){
        int result = random.nextInt() % max;
        if (result < 0)
            return -result;
        return result;
    }
}
