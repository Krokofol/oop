package sample.classes;

import javax.naming.InsufficientResourcesException;
import java.util.ArrayList;

class Bacteria {
    //направление взгляда
    private Integer gaze;
    //алгоритм
    private Algorithm algorithm;
    //энергия бактерии
    private Integer energy;
    //положение
    PhysicalObject physicalObject;
    //поле, общее
    private static Field field = new Field();


    //генератор случайных положительных чисел
    final Rand rand = new Rand();
    private Integer min(Integer a1, Integer a2) { return a2 > a1? a1: a2; }
    private Integer max(Integer a1, Integer a2) { return a1 > a2? a1: a2; }

    public Bacteria(){
        gaze = rand.getInteger(4);
        algorithm = new Algorithm();
        energy = 10;
    }
    public Bacteria(ArrayList<Bacteria> list){
        gaze = rand.getInteger(4);
        Integer index1 = rand.getInteger(list.size());
        Integer index2 = rand.getInteger(list.size());
        algorithm = new Algorithm(list.get(index1).algorithm, list.get(index2).algorithm);
        energy = 10;
    }

    //////////////////////////////////////////////////////////////////////////////////////////////
    //TURN
    private void turnUp() { this.gaze = 0; }
    private void turnRight() { this.gaze = 1; }
    private void turnDown() { this.gaze = 2; }
    private void turnLeft() { this.gaze = 3; }
    //////////////////////////////////////////////////////////////////////////////////////////////

    //////////////////////////////////////////////////////////////////////////////////////////////
    //PHYSICALOBJECT
    public PhysicalObject getPhysicalObject() { return physicalObject; }
    public void setPhysicalObject(PhysicalObject physicalObject) { this.physicalObject = physicalObject; }
    //////////////////////////////////////////////////////////////////////////////////////////////

    //////////////////////////////////////////////////////////////////////////////////////////////
    //MOVE
    private void moveUp(){
        field.setFieldElem(physicalObject.getX(), physicalObject.getY(), 0);
        physicalObject.setY(max(field.getSizeY(), physicalObject.getY() - 1));
        field.setFieldElem(physicalObject.getX(), physicalObject.getY(), 2);
    }
    private void moveDown(){
        field.setFieldElem(physicalObject.getX(), physicalObject.getY(), 0);
        physicalObject.setY(min(field.getSizeY(), physicalObject.getY() + 1));
        field.setFieldElem(physicalObject.getX(), physicalObject.getY(), 2);
    }
    private void moveLeft(){
        field.setFieldElem(physicalObject.getX(), physicalObject.getY(), 0);
        physicalObject.setX(max(field.getSizeX(), physicalObject.getX() - 1));
        field.setFieldElem(physicalObject.getX(), physicalObject.getY(), 2);
    }
    private void moveRight(){
        field.setFieldElem(physicalObject.getX(), physicalObject.getY(), 0);
        physicalObject.setX(min(field.getSizeX(), physicalObject.getX() + 1));
        field.setFieldElem(physicalObject.getX(), physicalObject.getY(), 2);
    }
    private void move(){
        switch (gaze){
            case (0) : moveUp();    break;
            case (1) : moveRight(); break;
            case (2) : moveDown();  break;
            case (3) : moveLeft();  break;
        }
        energy--;
        algorithm.moveIndex();
    }
    //////////////////////////////////////////////////////////////////////////////////////////////

    //////////////////////////////////////////////////////////////////////////////////////////////
    //EAT
    private void eatFood(){
        PhysicalObject physicalObject = new PhysicalObject();
        Integer x = this.physicalObject.getX();
        Integer y = this.physicalObject.getY();
        switch (gaze){
            case (0) : y--;
            case (1) : x++;
            case (2) : y++;
            case (3) : x--;
        }
        if (field.getFieldElem(x, y) == 1){
            energy += 5;
            return;
        }
        energy --;
    }
    private void eatBacteria(ArrayList<Bacteria> list){
        PhysicalObject physicalObject = new PhysicalObject();
        int i = 0;
        Integer x = this.physicalObject.getX();
        Integer y = this.physicalObject.getY();
        switch (gaze){
            case (0) : y--;
            case (1) : x++;
            case (2) : y++;
            case (3) : x--;
        }
        while (list.get(i).getPhysicalObject().getX().equals(x) && list.get(i).getPhysicalObject().getY().equals(y) && i < list.size()) i++;
        if (i != list.size()){
            field.setFieldElem(list.get(i).physicalObject.getX(), list.get(i).physicalObject.getY(), 0);
            list.remove(i);
            energy += 10;
        }
    }
    //////////////////////////////////////////////////////////////////////////////////////////////

    //////////////////////////////////////////////////////////////////////////////////////////////
    //IF
    private void check(){
        int res = 0;
        switch (gaze) {
            case (0) : res = checkUp(); break;
            case (1) : res = checkRight(); break;
            case (2) : res = checkDown(); break;
            case (3) : res = checkLeft(); break;
        }
        switch (res) {
            case (0) : resFree(); break;
            case (1) : resFood(); break;
            case (2) : resBact(); break;
        }
    }
    //CHECK
    private Integer checkUp(){
        return field.getFieldElem(this.physicalObject.getX(), max(this.physicalObject.getY() - 1, 0));
    }
    private Integer checkRight(){
        return field.getFieldElem(min(this.physicalObject.getX() + 1, field.getSizeX()), this.physicalObject.getY());
    }
    private Integer checkDown(){
        return field.getFieldElem(this.physicalObject.getX(), min(this.physicalObject.getY() + 1, field.getSizeY()));
    }
    private Integer checkLeft(){
        return field.getFieldElem(max(this.physicalObject.getX() - 1, 0), this.physicalObject.getY());
    }
    //RES
    private void resFree(){
        ArrayList<Integer> newIndexes = new ArrayList<Integer>();
        newIndexes.add((algorithm.getThisIndex() + 3) % 10);
        newIndexes.add((algorithm.getThisIndex() + 4) % 10);
        newIndexes.add((algorithm.getThisIndex() + 5) % 10);
        newIndexes.add((algorithm.getThisIndex() + 6) % 10);
        algorithm.setIndex(newIndexes);
    }
    private void resBact(){
        ArrayList<Integer> newIndexes = new ArrayList<Integer>();
        newIndexes.add((algorithm.getThisIndex() + 2) % 10);
        newIndexes.add((algorithm.getThisIndex() + 4) % 10);
        newIndexes.add((algorithm.getThisIndex() + 5) % 10);
        newIndexes.add((algorithm.getThisIndex() + 6) % 10);
        algorithm.setIndex(newIndexes);
    }
    private void resFood(){
        ArrayList<Integer> newIndexes = new ArrayList<Integer>();
        newIndexes.add((algorithm.getThisIndex() + 1) % 10);
        newIndexes.add((algorithm.getThisIndex() + 4) % 10);
        newIndexes.add((algorithm.getThisIndex() + 5) % 10);
        newIndexes.add((algorithm.getThisIndex() + 6) % 10);
        algorithm.setIndex(newIndexes);
    }
    //////////////////////////////////////////////////////////////////////////////////////////////

    //////////////////////////////////////////////////////////////////////////////////////////////
    public Boolean update(ArrayList<Bacteria> list){
        Integer action = this.algorithm.getAction();
        switch (action){
            case (0) : check(); break;
            case (1) : turnUp();            break;
            case (2) : turnRight();         break;
            case (3) : turnDown();          break;
            case (4) : turnLeft();          break;
            case (5) : move();              break;
            case (6) : eatFood();           break;
            case (7) : eatBacteria(list);   break;
        }
        Integer thisAction = 0;
        Integer nextAction = 0;
        return energy <= 0;
    }

}
