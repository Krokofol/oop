package sample.classes;

import java.util.ArrayList;

public class Field {
    private ArrayList<ArrayList<Integer>> field;
    private Integer sizeX = 20;
    private Integer sizeY = 10;

    final Rand rand = new Rand();


    /*
    Создаёт поле, в котором:
        15% вероятность создать бактерию        (2)
        35% вероятность создать еду             (1)
        50% вероятность создать пустую клетку   (0)
    */
    public Field(){
        Integer creating;
        this.field = new ArrayList<ArrayList<Integer>>();
        for (int i = 0; i < this.sizeX; i++) {
            this.field.add(new ArrayList<Integer>());
            for (int j = 0; j < this.sizeY; j++) {
                this.field.get(i).add(0);
                creating = rand.getInteger(100);
                if (50 <= creating && creating < 85) this.field.get(i).add(1);
                if (85 <= creating && creating < 100) this.field.get(i).add(2);
            }
        }
    }

    public void setFieldElem(Integer x, Integer y, Integer value) { this.field.get(x).set(y, value); }
    public Integer getFieldElem(Integer x, Integer y) { return this.field.get(x).get(y); }
    public Integer getSizeX() { return sizeX; }
    public Integer getSizeY() { return sizeY; }
}
