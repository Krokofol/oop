package sample;

import java.net.Inet4Address;
import java.util.ArrayList;

public class Holder <T extends Coordinates> {
    private ArrayList<ArrayList<T>> arrayLists;

    public Holder(){}

    private Integer findX(Coordinates element){
        int i = 0;
        int left = 0, right = this.arrayLists.size();
        boolean thisIsLessOrEq = false, rightIsBigger = false;

        while (!(thisIsLessOrEq && rightIsBigger)){
            i = (left + right) / 2;
            thisIsLessOrEq = this.arrayLists.get(i).get(0).getX() <= element.getX();
            rightIsBigger = true;
            if (this.arrayLists.size() != 1)
                rightIsBigger = this.arrayLists.get(i + 1).get(0).getX() > element.getX();

            if (thisIsLessOrEq) left = i;
            else right = i + 1;
        }
        return i;
    }
    private Integer findY(Coordinates element, Integer i){
        int j = 0;
        int left = 0, right = this.arrayLists.size();
        boolean thisIsLessOrEq = false, rightIsBigger = false;
        while (!(thisIsLessOrEq && rightIsBigger)){
            j = (left + right) / 2;
            thisIsLessOrEq = this.arrayLists.get(i).get(j).getY() <= element.getY();
            rightIsBigger = true;
            if (this.arrayLists.size() != 1)
                rightIsBigger = this.arrayLists.get(i).get(j + 1).getY() > element.getY();

            if (thisIsLessOrEq) left = j;
            else right = j + 1;
        }
        return j;
    }

    public boolean addElement(T element){
        //если массив пустой
        if (this.arrayLists == null){
            ArrayList<T> newList = new ArrayList<T>();
            newList.add(element);
            this.arrayLists = new ArrayList<ArrayList<T>>();
            this.arrayLists.add(newList);
            return false;
        }
        //если массив есть, но нам нужен столбец которого ещё нет
        int i, j;
        i = findX(element);
        if (addIfThereIsNotX(element, i)) return false;
        //если есть и массив и столбец, то вставляем в конкретное место
        j = findY(element, i);
        this.arrayLists.get(i).add(j + 1, element);
        return false;
    }
    public boolean checkPisition(Coordinates element){
        int i = this.findX(element);
        if (this.arrayLists.get(i).get(0).getX() != element.getX())
            return true;
        int j = this.findY(element, i);
        return this.arrayLists.get(i).get(j).getY() != element.getY();
    }
    public boolean moveElement(T element, Integer newX, Integer newY){
        int i, j;
        i = this.findX(element);
        j = this.findY(element, i);
        this.arrayLists.get(i).remove(j);
        element.setX(newX);
        element.setY(newY);
        i = this.findX(element);
        if (addIfThereIsNotX(element, i)) return false;
        j = this.findY(element, i);
        this.arrayLists.get(i).add(j + 1, element);
        return false;
    }

    private boolean addIfThereIsNotX(T element, int i) {
        if (this.arrayLists.get(i).get(0).getX() < element.getX()){
            ArrayList<T> newList = new ArrayList<T>();
            newList.add(element);
            this.arrayLists.add(i + 1, newList);
            return true;
        }
        return false;
    }
}
