package sample;

import java.util.ArrayList;

public class Bacteria extends Coordinates{
    private Integer energy;
    private ArrayList<Integer> actions;
    private Integer iteration;

    final Rand random = new Rand();

    public Bacteria() {
        this.energy = 50;
        this.actions = new ArrayList<Integer>();
        for (int i = 0; i < 300; i++){
            this.actions.add(random.getInteger(4));
        }
        this.iteration = -1;
    }
    public Integer getEnergy() {
        return energy;
    }
    public void setEnergy(Integer energy) {
        this.energy = energy;
    }
    public boolean loseEnergy(Integer count) {
        this.energy = this.energy - count;
        return this.energy < 1;
    }
    public boolean addEnergy(Integer count) {
        this.energy = this.energy + count;
        return false;
    }
    private void bigMoveUp(Holder<Bacteria> bacteriaHolder){
        this.moveUp();
        if (bacteriaHolder.checkPisition(this)) {
            this.moveDown();
            bacteriaHolder.moveElement(this, this.getX(), this.getY() - 1);
        }
    }
    private void bigMoveDown(Holder<Bacteria> bacteriaHolder){
        this.moveDown();
        if (bacteriaHolder.checkPisition(this)) {
            this.moveUp();
            bacteriaHolder.moveElement(this, this.getX(), this.getY() + 1);
        }
    }
    private void bigMoveLeft(Holder<Bacteria> bacteriaHolder){
        this.moveLeft();
        if (bacteriaHolder.checkPisition(this)) {
            this.moveRight();
            bacteriaHolder.moveElement(this, this.getX(), this.getY() - 1);
        }
    }
    private void bigMoveRight(Holder<Bacteria> bacteriaHolder){
        this.moveRight();
        if (bacteriaHolder.checkPisition(this)) {
            this.moveLeft();
            bacteriaHolder.moveElement(this, this.getX(), this.getY() - 1);
        }
    }
    public boolean update(Holder<Bacteria> bacteriaHolder, Holder<Food> foodHolder){
        iteration++;
        switch (this.actions.get(iteration)){
            case 0:
                bigMoveUp(bacteriaHolder);
                break;
            case 1:
                bigMoveRight(bacteriaHolder);
                break;
            case 2:
                bigMoveDown(bacteriaHolder);
                break;
            case 3:
                bigMoveLeft(bacteriaHolder);
                break;
        }
        if (!foodHolder.checkPisition(this))
            energy += 10;
        return this.loseEnergy(1);
    }
}
