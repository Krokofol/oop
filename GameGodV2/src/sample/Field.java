package sample;

public class Field {
    private Integer xSize;
    private Integer ySize;

    public Field(){
        this.xSize = 200;
        this.ySize = 100;
    }

    public Integer getxSize() {
        return xSize;
    }
    public Integer getySize() {
        return ySize;
    }
}
